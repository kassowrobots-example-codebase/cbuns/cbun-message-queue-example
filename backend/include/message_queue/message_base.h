/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2023, KR Soft s.r.o.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Kassow Robots nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#ifndef KR2_MESSAGE_BASE_H
#define KR2_MESSAGE_BASE_H

#include <message_queue/message_time_utils.h>
#include <sys/msg.h>
#include <sys/types.h>
#include <cerrno>

#define TIMEOUT_SLEEP_MS 1

namespace kswx_message_queue
{
    enum class result
    {
        SUCCESS = 0,

        /// @brief queue is already initialized, cannot be re-initialized
        ALREADY_INITIALIZED = -1,

        /// @brief failure to create or connect to existing queue
        INITIALIZATION_ERROR = -2,

        /// @brief communication error during sending or receiving messages
        COMMUNICATION_ERROR = -3,

        /// @brief queue or channel is in an invalid state
        QUEUE_INVALID = -4,

        /// @brief timeout occurred
        TIMEOUT = -5,

        /// @brief no message on non-blocking receive
        NO_MESSAGE = -6,

        /// @brief channel is not open
        CHANNEL_NOT_OPEN = -7
    };

    inline const char* resultToString(result t)
    {
        switch (t)
        {
            case result::SUCCESS: return "SUCCESS";
            case result::ALREADY_INITIALIZED: return "ALREADY_INITIALIZED";
            case result::INITIALIZATION_ERROR: return "INITIALIZATION_ERROR";
            case result::COMMUNICATION_ERROR: return "COMMUNICATION_ERROR";
            case result::QUEUE_INVALID: return "QUEUE_INVALID";
            case result::TIMEOUT: return "TIMEOUT";
            case result::NO_MESSAGE: return "NO_MESSAGE";
            case result::CHANNEL_NOT_OPEN: return "CHANNEL_NOT_OPEN";
            default: return "UNKNOWN_ERROR_TYPE";
        }
    }

    /// @brief base class for client <-> server message queue communication, contains message send/receive wrappers
    class MessageBase
    {
    public:
        MessageBase();
        virtual ~MessageBase();

    protected:
        /// @brief Send message to queue within a specified deadline. Blocking if deadline=0, re-try until deadline if deadline > 0.
        /// Always tries to send at least once. Does not check queue validity.
        /// @param buf struct for msgsnd - containing mtype and payload to send
        /// @param deadline 0 for a blocking call (waiting on a non-full queue), >0 for re-try until deadline
        /// @return SUCCESS, COMMUNICATION_ERROR or TIMEOUT
        template <typename send_msgbuf>
        result sendWithDeadline(send_msgbuf buf, uint64_t deadline);

        /// @brief Receive a message from queue within a specified deadline. Blocking if deadline=0, re-try until deadline if deadline > 0.
        /// Always tries to receive at least once. Does not check queue validity.
        /// @param buf struct for msgrcv - containing mtype and payload to receive
        /// @param deadline 0 for a blocking call (waiting on a non-empty queue), >0 for re-try until deadline, 1 for non-blocking single try
        /// @param msg_type channel type to receive the message from
        /// @return SUCCESS, COMMUNICATION_ERROR, NO_MESSAGE (for deadline = 1) or TIMEOUT
        template <typename receive_msgbuf>
        result receiveWithDeadline(receive_msgbuf &buf, uint64_t deadline, long msg_type);

        /// @brief Calculate deadline from timeout.
        /// @param timeout_ms timeout in milliseconds
        /// @return 0 if timeout is 0, the current time in milliseconds since epoch + timeout otherwise.
        static uint64_t getDeadline(long timeout_ms);

        int queue_id;
    };

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    template <typename send_msgbuf>
    inline result MessageBase::sendWithDeadline(send_msgbuf buf, uint64_t deadline)
    {
        int err;
        if (deadline == 0)
        {
            err = msgsnd(queue_id, &buf, buf.size(), 0);
            if (err == -1)
                return result::COMMUNICATION_ERROR;
        }
        else
        {
            while (true)
            {
                err = msgsnd(queue_id, &buf, buf.size(), IPC_NOWAIT);
                if (err == 0)
                    return result::SUCCESS;
                if (errno != EAGAIN)
                    return result::COMMUNICATION_ERROR;
                if (millis() > deadline)
                    return result::TIMEOUT;
                sleepMillis(TIMEOUT_SLEEP_MS);
            }
        }

        return result::SUCCESS;
    }

    template <typename receive_msgbuf>
    inline result MessageBase::receiveWithDeadline(receive_msgbuf &buf, uint64_t deadline, long msg_type)
    {
        ssize_t byte_count;
        if (deadline == 1)
        {
            byte_count = msgrcv(queue_id, &buf, buf.size(), msg_type, IPC_NOWAIT);
            if (byte_count != buf.size())
            {
                if (errno == ENOMSG)
                    return result::NO_MESSAGE;
                else
                    return result::COMMUNICATION_ERROR;
            }
        }
        else if (deadline == 0)
        {
            byte_count = msgrcv(queue_id, &buf, buf.size(), msg_type, 0);
            if (byte_count != buf.size())
                return result::COMMUNICATION_ERROR;
        }
        else
        {
            while (true)
            {
                byte_count = msgrcv(queue_id, &buf, buf.size(), msg_type, IPC_NOWAIT);
                if (byte_count == buf.size())
                    return result::SUCCESS;
                if (errno != ENOMSG)
                    return result::COMMUNICATION_ERROR;
                if (millis() > deadline)
                    return result::TIMEOUT;
                sleepMillis(TIMEOUT_SLEEP_MS);
            }
        }

        return result::SUCCESS;
    }
}

#endif // KR2_MESSAGE_BASE_H