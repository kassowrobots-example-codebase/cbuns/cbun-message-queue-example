/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2023, KR Soft s.r.o.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Kassow Robots nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#ifndef KR2_MESSAGE_CLIENT_H
#define KR2_MESSAGE_CLIENT_H

#include <message_queue/message_base.h>
#include <message_queue/message_structs.h>

namespace kswx_message_queue
{
    /// @brief class for client -> server communication, initialize a channel, send requests and receive responses
    /// @tparam req_t type of client->server message (request)
    /// @tparam resp_t type of server->client message (response)
    template <typename req_t, typename resp_t>
    class MessageClient : MessageBase
    {
    public:
        MessageClient();

        /// @brief Send a request to the server to close the connection.
        virtual ~MessageClient();

        /// @brief Connect to an existing queue and initialize a channel for communication.
        /// @param key identifies the queue, usually received from an ftok call
        /// @param timeout_ms 0 for no timeout (blocking call), >0 for timeout in milliseconds
        /// @return SUCCESS, ALREADY_INITIALIZED, INITIALIZATION_ERROR, COMMUNICATION_ERROR or TIMEOUT
        result connect(key_t key, long timeout_ms);

        /// @brief Send a request to the server, blocking if timeout = 0, with timeout if timeout_ms > 0
        /// @param timeout_ms timeout in milliseconds or 0 for a blocking call
        /// @return SUCCESS, QUEUE_INVALID, COMMUNICATION_ERROR or TIMEOUT
        result send(req_t request, long timeout_ms);

        /// @brief Receive a response from the server, blocking if timeout = 0, with timeout if timeout_ms > 0
        /// @param timeout_ms timeout in milliseconds or 0 for a blocking call
        /// @return SUCCESS, QUEUE_INVALID, COMMUNICATION_ERROR or TIMEOUT
        result receive(resp_t& response, long timeout_ms);
        
    private:
        long channel_id;
    };





    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////





    template <typename req_t, typename resp_t>
    inline MessageClient<req_t, resp_t>::MessageClient() : channel_id(-1) {}
    
    template <typename req_t, typename resp_t>
    inline MessageClient<req_t, resp_t>::~MessageClient()
    {
        long deadline = getDeadline(10);
        channel_request_msgbuf req(channel_id);
        sendWithDeadline(req, deadline);
    }
    
    template <typename req_t, typename resp_t>
    inline result MessageClient<req_t, resp_t>::connect(key_t key, long timeout_ms)
    {
        long deadline = getDeadline(timeout_ms);

        if (queue_id != -1 || channel_id != -1)
            return result::ALREADY_INITIALIZED;

        int err = msgget(key, 0600);

        if (err == -1)
            return result::INITIALIZATION_ERROR;

        queue_id = err;

        result res;

        channel_request_msgbuf req;
        res = sendWithDeadline(req, deadline);
        if (res != result::SUCCESS)
            return res;

        channel_response_msgbuf resp;
        res = receiveWithDeadline(resp, deadline, resp.mtype);
        if (res != result::SUCCESS)
            return res;
        
        channel_id = resp.channel_id;

        return result::SUCCESS;
    }

    template <typename req_t, typename resp_t>
    inline result MessageClient<req_t, resp_t>::send(req_t request, long timeout_ms)
    {
        long deadline = getDeadline(timeout_ms);

        if (queue_id == -1 || channel_id == -1)
            return result::QUEUE_INVALID;

        message_request_msgbuf<req_t> req_buf(channel_id, request);

        result res = sendWithDeadline(req_buf, deadline);
        if (res != result::SUCCESS)
            return res;

        return result::SUCCESS;
    }
    
    template <typename req_t, typename resp_t>
    inline result MessageClient<req_t, resp_t>::receive(resp_t &response, long timeout_ms)
    {
        long deadline = getDeadline(timeout_ms);

        if (queue_id == -1 || channel_id == -1)
            return result::QUEUE_INVALID;

        message_response_msgbuf<resp_t> resp_buf;

        result res = receiveWithDeadline(resp_buf, deadline, channel_id);
        if (res != result::SUCCESS)
            return res;

        response = resp_buf.payload;

        return result::SUCCESS;
    }
}

#endif // KR2_MESSAGE_CLIENT_H